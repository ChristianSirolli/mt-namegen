
local S = namegen.S



minetest.register_chatcommand( "test_namegen", {
	description = S("Test the name generator."),
	params = S("[<generator name>]"),
	func = function( player_name , param )
		local name = namegen.generate( param , true )
		if not name then
			return false, S("Can't generate a name using generator @1", param)
		end
		
		return true, S("Name: @1", name)
	end
} )

